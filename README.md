# Simple POV Display using APA102 RGB LED STRIP and Arduino NANO

This is a Simple POV Display using APA102 RGB LED STRIP and Arduino NANO

**Materials needed:**

APA102 RGB LED STRIP

Arduino NANO

Hall Sensor

3.7V Battery

ON OFF Switch

PCB

8mm x 3mm thickness neodymium magnet

DC Motor

![Untitled](img/main.png)

[https://www.youtube.com/watch?v=KqtQpt8MiYM&t=336s](https://www.youtube.com/watch?v=KqtQpt8MiYM&t=336s)

You can download Arduino Code here.Both the codes are same in working but written in different ways.