
// hobbyprojects.com
// APA102_RGB_LED_Simple_POV_Display1.ino

int sensor_pin = A0;
int apa102Clock = 3;
int apa102Data = 4;

int input_state;
int i,k;

void setup() {
  pinMode(sensor_pin, INPUT_PULLUP);   
  pinMode(apa102Data,OUTPUT);  
  pinMode(apa102Clock,OUTPUT);  
}

//---------------------------

void loop()
{

    input_state = digitalRead(sensor_pin);
    while (input_state != 0)
  {
    input_state = digitalRead(sensor_pin);    
  }
  
//------------------------------------------------

// char 'R' 1st Column

  // Start Frame
  
  spiWrite(0x00);   
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED1 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED1 green OFF)
  spiWrite(0b11111111);  // 1st column, LED1 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED2 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED2 green OFF)
  spiWrite(0b11111111);  // 1st column, LED2 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED3 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED3 green OFF)
  spiWrite(0b11111111);  // 1st column, LED3 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED4 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED4 green OFF)
  spiWrite(0b11111111);  // 1st column, LED4 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED5 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED5 green OFF)
  spiWrite(0b11111111);  // 1st column, LED5 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED6 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED6 green OFF)
  spiWrite(0b11111111);  // 1st column, LED6 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED7 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED7 green OFF)
  spiWrite(0b11111111);  // 1st column, LED7 red ON)


  // End frame
  spiWrite(0b11111111);

//======

  // blank Column
  
  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);


  //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 
   // End frame
  spiWrite(0b11111111);

//===========================================================================

// char 'R' 2nd Column


  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED1 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED1 green OFF)
  spiWrite(0b00000000);    // 2nd column, LED1 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED2 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED2 green OFF)
  spiWrite(0b00000000);    // 2nd column, LED2 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED3 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED3 green OFF)
  spiWrite(0b00000000);    // 2nd column, LED3 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED4 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED4 green OFF)
  spiWrite(0b11111111);    // 2nd column, LED4 red ON)

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED5 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED5 green OFF)
  spiWrite(0b00000000);    // 2nd column, LED5 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED6 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED6 green OFF)
  spiWrite(0b00000000);    // 2nd column, LED6 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);    // 2nd column, LED7 blue OFF)
  spiWrite(0b00000000);    // 2nd column, LED7 green OFF)
  spiWrite(0b11111111);    // 2nd column, LED7 red ON)


//---------

  // End frame
  
  spiWrite(0b11111111);

//======

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

  //LED frames  

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);

//===========================================================================

// char 'R' 3rd Column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

//---------

  // End frame
  spiWrite(0b11111111);

//---------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

  //LED frames  

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);



//===========================================================================

// char 'R' 4th Column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);


//---------

  // End frame
  spiWrite(0b11111111);


//---------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

  //LED frames  

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//===========================================================================

// char 'R' 5th Column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
//
  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b11111111);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);


//---------

  // End frame
  spiWrite(0b11111111);


//---------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

  //LED frames
  
  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//===========================================================================

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

  //LED frames
  
  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);
  
//---------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

  //LED frames
  
  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);

//===========================================================================
//===========================================================================

  //char 'G' 1st column 
  
  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED1 blue OFF)
  spiWrite(0b00000000);  // 1st column, LED1 green OFF)
  spiWrite(0b00000000);  // 1st column, LED1 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);  // 1st column, LED2 blue OFF)
  spiWrite(0b11111111);  // 1st column, LED2 green ON)
  spiWrite(0b00000000);  // 1st column, LED2 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);


//---------
  // End frame
  spiWrite(0b11111111); 


//---------

// blank Column

  // Start Frame 
  spiWrite(0x00); 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

   //LED frames 

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//===========================================================================

  //char 'G' 2nd column 

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  // End frame
  spiWrite(0b11111111);


//-------------------------

// blank Column


  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

   //LED frames 

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//===========================================================================

  //char 'G' 3rd column 

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);


  //LED frames
  
  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);


//---------

  // End frame
  spiWrite(0b11111111);

//-------------------------

// blank Column
  

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//===========================================================================

  //char 'G' 4th column 

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  //LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);


//---------

  // End frame
  spiWrite(0b11111111);

 //-------------------------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);
 
//===========================================================================

  //char 'G' 5th column 

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  //LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
//
  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b11111111);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

//---------

  // End frame
  spiWrite(0b11111111);


//-------------------------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111); 


//===========================================================================

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111); 

  
//-------------------------

  //Blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111); 

//===========================================================================
//===========================================================================

//char 'B' 1st column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  // LED frames

  spiWrite(0b11100011);
  spiWrite(0b11111111);  // 1st column, LED1 blue ON)
  spiWrite(0b00000000);  // 1st column, LED1 green OFF)
  spiWrite(0b00000000);  // 1st column, LED1 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b11111111);  // 1st column, LED2 blue ON)
  spiWrite(0b00000000);  // 1st column, LED2 green OFF)
  spiWrite(0b00000000);  // 1st column, LED2 red OFF)

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

//---------
  // End frame
  spiWrite(0b11111111);

//-------------------------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);

//===========================================================================

//char 'B' 2nd column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  //LED frames

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

//---------

  // End frame
  spiWrite(0b11111111);

//-------------------------

// blank Column
  
  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111); 

//===========================================================================

//char 'B' 3rd column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  //LED frames

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

//---------

  // End frame
  spiWrite(0b11111111);

//-------------------------

// blank Column
  
  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111); 


//===========================================================================

//char 'B' 4th column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  //LED frames

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

//---------

  // End frame
  spiWrite(0b11111111);

//-------------------------

// blank Column
  
  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//===========================================================================

//char 'B' 5th column

  spiWrite(0x00);  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

//---------

  //LED frames

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b11111111);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

  spiWrite(0b11100011);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);

//---------
  // End frame
  spiWrite(0b11111111);

//-------------------------

// blank Column

  // Start Frame 
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);
  spiWrite(0x00);

    //LED frames

  for(k = 0; k < 7; k++)
{  

  spiWrite(0b11100000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
  spiWrite(0b00000000);
 
} 

   // End frame
  spiWrite(0b11111111);


//============


    input_state = digitalRead(sensor_pin);
    while (input_state == 0)
  {
    input_state = digitalRead(sensor_pin);    
  }  

}

//===========================================================================

void spiWrite(uint8_t c) {
  uint8_t i;
  for (i=0; i<8 ;i++)
  {
    if (!(c&0x80)) {
      digitalWrite(apa102Data, LOW);
    } else {
      digitalWrite(apa102Data, HIGH);
    }     
  
      digitalWrite(apa102Clock, HIGH);

  c <<= 1;
  

      digitalWrite(apa102Clock, LOW);
  }

      digitalWrite(apa102Data, HIGH);
}
